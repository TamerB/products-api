Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :products, except: [:create]
  resources :departments do
    resources :products, only: [:create]
    get 'products', to: 'products#department_products'
  end
  resources :promotions do
    get 'products', to: 'products#promotion_products'
    put 'add_promotion/:id', to: 'products#add_promotion'
    delete 'remove_promotion/:id', to: 'products#remove_promotion'
  end

  get 'search/:search', to: 'products#search'
  get 'pages/', to: 'products#pages'

  get 'dep_pages/:id', to: 'departments#products_count'
  get 'pro_pages/:id', to: 'promotions#products_count'
end