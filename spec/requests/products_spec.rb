require 'rails_helper'

RSpec.describe 'Products API' do
  # Initialize the test data
  let!(:department) { create(:department) }
  let!(:promotion) { create(:promotion) }
  let!(:products) { create_list(:product, 30, department_id: department.id) }
  let(:department_id) { department.id }
  let(:promotion_id) { promotion.id }
  let(:id) { products.first.id }

  # Test suite for GET /products
  describe 'GET /products' do
    before { get "/products" }

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns all department products' do
      expect(json.size).to eq(20)
    end
  end

  # Test suite for GET /departments/:department_id/department_products
  describe 'GET /departments/:department_id/department_products' do
    before { get "/departments/#{department_id}/department_products" }

    context 'when department exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all department products' do
        expect(json.size).to eq(20)
      end
    end

    context 'when department does not exist' do
      let(:department_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Department with 'id'=0/)
      end
    end
  end

  # Test suite for GET /promotions/:promotion_id/promotion_products
  describe 'GET /promotions/:promotion_id/promotion_products' do
    before { get "/promotions/#{promotion_id}/promotion_products" }

    context 'when promotion exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all promotion products' do
        expect(json.size).to eq(0)
      end
    end

    context 'when promotion does not exist' do
      let(:promotion_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Promotion with 'id'=0/)
      end
    end
  end

  # Test suite for GET /products/:id
  describe 'GET /products/:id' do
    before { get "/products/#{id}" }

    context 'when product exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the product' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when product does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Product/)
      end
    end
  end

  # Test suite for POST /departments/:department_id/products
  describe 'POST /departments/:department_id/products' do
    let(:valid_attributes) { { name: 'Some Product', price: 10 } }

    context 'when request attributes are valid' do
      before { post "/departments/#{department_id}/products", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/departments/#{department_id}/products", params: {name: "Some Product"} }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Price can't be blank/)
      end
    end
  end

  # Test suite for PUT /departments/:department_id/products/:id
  describe 'PUT /products/:id' do
    let(:valid_attributes) { { name: 'Mozart' } }

    before { put "/products/#{id}", params: valid_attributes }

    context 'when product exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the product' do
        updated_product = Product.find(id)
        expect(updated_product.name).to match(/Mozart/)
      end
    end

    context 'when the product does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Product/)
      end
    end
  end

  # Test suite for DELETE /products/:id
  describe 'DELETE /products/:id' do
    before { delete "/products/#{id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end