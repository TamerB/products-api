require 'rails_helper'

RSpec.describe Promotion, type: :model do
  # Association test
  # ensure an product record has many promotion records
  it { should have_many(:products) }
  # Validation test
  # ensure columns name and price are present before saving
  it { should validate_presence_of(:code) }
  it { should validate_presence_of(:active) }
  it { should validate_presence_of(:discount) }
end
