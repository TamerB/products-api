require 'rails_helper'

RSpec.describe Department, type: :model do
  it { should have_many(:products).dependent(:destroy) }
  # Validation tests
  # ensure column name is present before saving
  it { should validate_presence_of(:name) }
end