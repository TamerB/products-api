require 'rails_helper'

RSpec.describe Product, type: :model do
  # Association test
  # ensure an product record belongs to a single department record
  it { should belong_to(:department) }
  # Association test
  # ensure an product record has many promotion records
  it { should have_many(:promotions) }
  # Validation test
  # ensure columns name and price are present before saving
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:price) }
end
