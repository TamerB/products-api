require 'rails_helper'

RSpec.describe ProductsPromotion, type: :model do
  # Association test
  # ensure an products promotion record belongs to a single product record and a single promotion record
  it { should belong_to(:product) }
  it { should belong_to(:promotion) }
end
