FactoryBot.define do
  factory :product do
    name { Faker::Lorem.word }
    price { Faker::Number.number(digits: 2)  }
    department_id { nil }
  end
end