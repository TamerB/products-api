FactoryBot.define do
  factory :promotion do
    code { Faker::Lorem.word }
    active { true }
    discount { Faker::Number.number(digits: 2) }
  end
end