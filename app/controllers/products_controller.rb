class ProductsController < ApplicationController
  before_action :set_department, only: [:department_products, :create]
  before_action :set_department_product, only: [:show, :update, :destroy, :add_promotion, :remove_promotion]
  before_action :set_promotion, only: [:add_promotion, :remove_promotion, :promotion_products]

  # Independent methods

  # GET /products
  def index
    @products = Product.all.paginate(page: params[:page], per_page: 5)
    json_response(@products)
  end

  def pages
    @count = Product.all.count
    @count = (@count / 5) + (@count % 5 > 0 ? 1 : 0)
    json_response(@count)
  end

  # GET /products/:id
  def show
    json_response(@product)
  end

  # PUT /products/:id
  def update
    @product.update(product_params)
    head :no_content
  end

  # DELETE /products/:id
  def destroy
    @product.destroy
    head :no_content
  end

  def search
    @products = Product.search(params[:search])
    json_response(@products)
  end

  # Department dependent methods

  # GET /departments/:department_id/department_products
  def department_products
    json_response(@department.products.paginate(page: params[:page], per_page: 5))
  end

  # POST /departments/:department_id/products
  def create
    @product = @department.products.create!(product_params)
    json_response(@product, :created)
  end

  # Promotion dependent methods

  # GET /promotion/:promotion_id/promotion_products
  def promotion_products
    json_response(@promotion.products.paginate(page: params[:page], per_page: 5))
  end

  # POST /promotions/:promotion_id/add_promotion/:id
  def add_promotion
    unless @product.promotions.include? @promotion
      @product.promotions << @promotion
      head :no_content
    end
  end

  # DELETE /promotions/:promotion_id/remove_promotion/:id
  def remove_promotion
    ProductsPromotion.where(product: @product, promotion: @promotion).first.destroy
  end

  private

  def product_params
    params.permit(:name, :price, :department)
  end

  def set_department
    @department = Department.find(params[:department_id])
  end

  def set_department_product
    @product = Product.find(params[:id])
  end

  def set_promotion
    @promotion = Promotion.find(params[:promotion_id])
  end
end