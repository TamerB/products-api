class Promotion < ApplicationRecord
  # model association
  has_many :products_promotions, dependent: :destroy
  has_many :products, through: :products_promotions

  # validations
  validates_presence_of :code, :active, :discount
end
