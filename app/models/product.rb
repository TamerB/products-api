class Product < ApplicationRecord
  # model association
  belongs_to :department
  has_many :products_promotions, dependent: :destroy
  has_many :promotions, through: :products_promotions

  # validations
  validates_presence_of :name, :price

  def self.search(search)
    if search
      all.where('lower(name) LIKE ?', "%#{search}%")
    else
      all
    end
  end
end
