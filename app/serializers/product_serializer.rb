class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :price

  belongs_to :department
  has_many :promotions
end
