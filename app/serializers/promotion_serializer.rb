class PromotionSerializer < ActiveModel::Serializer
  attributes :id, :code, :active, :discount

  has_many :products
end
