# README

This is a simple Ruby on Rails API that provides the following functionalities.
* CRUD operations to "Product", "Department", and "Promotion".
* Response Pagination.
* Simple search by Product name.
* Products filter by Department or Promotion.

## Ruby version

This is app uses `ruby-2.6.5`.

## Configuration

When running the app localy, please make sure that the port it's running on is not consumed by another app.

This app uses `rails-dotenv` to store database username and password. So you need to add a `.env` file with the required information in it.

## Database creation

This app uses MariaDB.

## Database initialization

Please run `rails db:create` to create the databases and `rails db:migrate` to run migrations.

## How to run the test suite

You can run the tests with `bundle exec rspec`.

## APIs provided:

Please run the command `rails routes` to view the available API routes.
